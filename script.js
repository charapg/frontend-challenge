function showPassword(id) 
{
    var x = document.getElementById(id);
    x.setAttribute('type', 'text');
    x = document.getElementById(id+'Text');
    x.setAttribute('onclick','hidePassword(\'' + id + '\');');
    x.textContent = 'Hide Password';
}

function hidePassword(id) 
{
    var x = document.getElementById(id);
    x.setAttribute('type', 'password');
    x = document.getElementById(id+'Text');
    x.setAttribute('onclick','showPassword(\'' + id + '\');');
    x.textContent = 'Show Password';
}

function setDisplayMode(id, mode)
{
	document.getElementById(id).style.display = mode;
}

function validateForm()
{   
    var isEmpty = 4;
    var passwordMatch = true;
    var i;
    

    for(i = 0; i < 4; i++ )
    { 
        var aux = document.forms['form001'][i].value;
        if(aux == "")
        {
            document.forms['form001'][i].style.borderColor= 'red';
            isEmpty;
        }
        else
        {
           document.forms['form001'][i].style.borderColor= '#4e4e4e';
           isEmpty--;
        }
    }

    var password = document.forms['form001']['password'].value;
    var passwordConfirmation = document.forms['form001']['passwordConfirmation'].value;
    if(password == passwordConfirmation && (password + passwordConfirmation != ''))
    {
        document.forms['form001']['password'].style.borderColor= '#4e4e4e';
        document.forms['form001']['passwordConfirmation'].style.borderColor= '#4e4e4e';
        passwordMatch = true;
    }
    else if(password + passwordConfirmation != '')
    {
        document.forms['form001']['password'].style.borderColor= 'orange';
        document.forms['form001']['passwordConfirmation'].style.borderColor= 'orange';
        passwordMatch = false;
    }

    if(!isEmpty && passwordMatch)
    {
        hidePassword('psw');
        hidePassword('pswCfm');
        document.getElementById('form01').submit();
        setDisplayMode('label','none');
    }
    else
    {
        setDisplayMode('label', 'block');
    }

}

