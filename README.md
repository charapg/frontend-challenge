# Desafio Elevential

1. O que você achou do desafio?
	Achei o desafio bem tranquilo.

2. Quais foram as maiores dificuldades encontradas?
	A falta de prática, pois eu não lembrava de grande parte dos comandos. Algo que foi contornado facilmente.

3. O que poderia ser melhorado no resultado?
	Acho que faltaram algumas informações de posicionamento dos elementos. Isso ajudaria num melhor resultado.

4. Quais funcionalidades você julgou mais importantes e por quê?
	A funcionalidade de mostrar e esconder as senhas, pois foi a que eu levei mais tempo para implementar.

5. Que decisões você tomou e por quê?
	Sobre itens de escolha livre das especificações, eu decidi usar cores nas linhas dos campos de entrada para sinalizar erro pois acho que seja um modo de afetar minimamente o layout original do projeto. E, também decidi não fazer a validação do conteúdo dos campos, pois não tinha nada especificado para tal tarefa.